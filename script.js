//mapa labirinto
const map = [
  "WWWWWWWWWWWWWWWWWWWWW",
  "W   W     W     W W W",
  "W W W WWW WWWWW W W W",
  "W W W   W     W W   W",
  "W WWWWWWW W WWW W W W",
  "W         W     W W W",
  "W WWW WWWWW WWWWW W W",
  "W W   W   W W     W W",
  "W WWWWW W W W WWW W F",
  "S     W W W W W W WWW",
  "WWWWW W W W W W W W W",
  "W     W W W   W W W W",
  "W WWWWWWW WWWWW W W W",
  "W       W       W   W",
  "WWWWWWWWWWWWWWWWWWWWW",
];


//REQUISISTOS
//1. ponto de inicio, S
//2. handlers de evento para mover a div jogagor
//3. paredes intrasponíveis
//4. condição de vitória (!alert)

//ESTRUTURA
//1. cada campo do labirinto é representado por uma div 
//2. cada linha do labirinto uma div
//3. OU div jogador psa OU S append div jogador   
//4. registrar a posição atual do jogador no labirinto (índices de linha e de coluna)
//5. movimento, obter o próximo elemento de célula pelos índices via atributos de dados e append


/* ESTRUTURA DO LABIRINTO */

let mazeContainer = document.createElement("div");
mazeContainer.className = "maze";
document.body.appendChild(mazeContainer);

const hero = document.createElement("div")
hero.id = "hero"
hero.style.background = "black"
hero.style.height = "20px"
hero.style.width = "20px"

const victory = document.querySelector("#victory")

for (let row in map) {
  const mazeRows = document.createElement("div");
  mazeRows.className = "maze-rows";
  mazeContainer.appendChild(mazeRows);

  for (let col in map[row]) {
    let mazeCell = document.createElement("div");
    mazeCell.dataset.row = row
    mazeCell.dataset.col = col

    switch (map[row][col]) {
      case "S":
        mazeCell.className = "maze-cell start";
        mazeCell.appendChild(hero)
        break;
      case "F":
        mazeCell.className = "maze-cell finish";
        break;
      case "W":
        mazeCell.className = "maze-cell wall";
        break;
      case " ":
        mazeCell.className = "maze-cell path";
        break;
    }
    mazeRows.appendChild(mazeCell);
  }
}


/* MOVIMENTAÇÃO */

document.addEventListener('keydown', (event) => {
  const keyName = event.key;

  let heroCurrentRow = Number(hero.parentElement.dataset.row);
  let heroCurrentCol = Number(hero.parentElement.dataset.col);

  let destination = mazeContainer.children[heroCurrentRow].children[heroCurrentCol]

  console.log(destination)

  if (keyName === 'ArrowUp') {
    destination = mazeContainer.children[heroCurrentRow - 1].children[heroCurrentCol];
  }
  if (keyName === 'ArrowDown') {
    destination = mazeContainer.children[heroCurrentRow + 1].children[heroCurrentCol];
  }
  if (keyName === 'ArrowRight') {
    destination = mazeContainer.children[heroCurrentRow].children[heroCurrentCol + 1];
  }
  if (keyName === 'ArrowLeft') {
    destination = mazeContainer.children[heroCurrentRow].children[heroCurrentCol - 1];
  }

  let wall = destination.className.includes("wall");
  let finish = destination.className.includes("finish");

  if (destination) {
    if (!wall) {
      destination.appendChild(hero)
    } 
    if (finish) {
/*       document.querySelector('.finish').appendChild(victory)
 */      victory.style.visibility = "visible"
      
 }
}

  console.log(destination)
  console.log(wall)
  console.log(finish)

});

const reset = () => {
  document.querySelector('.start').appendChild(hero);
  victory.style.visibility = "hidden"
} 
victory.addEventListener("click", reset)